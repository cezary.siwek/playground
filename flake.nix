{
  description = "A flake";

  outputs = { self, nixpkgs }: {
    hello = 
    with import nixpkgs { system = "x86_64-linux"; };
    stdenv.mkDerivation {
      name = "hello-2.3";

      src = fetchurl {
        url = mirror://gnu/hello/hello-2.3.tar.bz2;
        sha256 = "0c7vijq8y68bpr7g6dh1gny0bff8qq81vnp4ch8pjzvg56wb3js1";
      };
    };

    defaultPackage.x86_64-linux =
      # Notice the reference to nixpkgs here.
      with import nixpkgs { system = "x86_64-linux"; };
      stdenv.mkDerivation {
        name = "playground";
        src = self;

        buildInputs = [ hello which ];

        installPhase = ''
          mkdir -p $out/bin;
          mkdir -p $out/share;
          install -t $out/share/ ./src/playground.sh
          echo "#! ${stdenv.shell}" >> "$out/bin/hello"
          echo "exec $(which hello)" >> "$out/bin/hello"
        '';

        shellHook = ''
          env
          export TEST=WTF
        '';
      };
   };
}
